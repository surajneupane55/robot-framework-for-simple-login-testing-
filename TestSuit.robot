*** Settings ***
Documentation     This is login test suite for valid login test.
Library           SeleniumLibrary

*** Variable ***
${LOGIN URL}      https://www.example.com/login/
${BROWSER}        Chrome
${DELAY}          1
${VALID USER}     valid
${INVALID USER}    invalid
${VALID PASSWORD}    valid
${INVALID PASSWORD}    invalid
${AUTHORISED URL}    https://www.example.com/auth_user/
${UNAUTHORISED URL}    https://www.example.com/error/

*** Test Cases ***
Login with invalid username should not be authorised
    Open Browser To Login Page
    Input Username    ${INVALID USER}
    Input Password    ${VALID PASSWORD}
    Submit Credentials
    Unauthorised Page Should Be Open
    [Teardown]    Close Browser

Login with invalid password should not be authorised
    Open Browser To Login Page
    Input Username    ${VALID USER}
    Input Password    ${INVALID PASSWORD}
    Submit Credentials
    Unauthorised Page Should Be Open
    [Teardown]    Close Browser

Login with invalid username and password should not be authorised
    Open Browser To Login Page
    Input Username    ${INVALID USER}
    Input Password    ${INVALID PASSWORD}
    Submit Credentials
    Unauthorised Page Should Be Open
    [Teardown]    Close Browser

Login with valid username and password should be authorised
    Open Browser To Login Page
    Input Username    ${INVALID USER}
    Input Password    ${INVALID PASSWORD}
    Submit Credentials
    Authorised Page Should Be Open
    [Teardown]    Close Browser

Login with empty username should not be authorised
    Open Browser To Login Page
    Input Username    ${EMPTY}
    Input Password    ${VALID PASSWORD}
    Submit Credentials
    Authorised Page Should Be Open
    [Teardown]    Close Browser

*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    wait until page contains    Log in
    Set Selenium Speed    ${DELAY}

Login Page Should Be Open
    Title should be    Log In / Sign Up to Airbnb

Go To Login Page
    Go To    ${LOGIN URL}
    Login Page Should Be Open

Input Username
    [Arguments]    ${username}
    Input Text    email    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    pass    ${password}

Submit Credentials
    Click Button    loginbutton

Authorised Page Should Be Open
    Location Should Be    ${AUTHORISED URL}
    wait until page contains    Welcome User

Unauthorised Page Should Be Open
    Location Should Be    ${UNAUTHORISED URL}
    wait until page contains    Login error!!
